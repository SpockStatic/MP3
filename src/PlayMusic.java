import javazoom.jl.decoder.Bitstream;
import javazoom.jl.decoder.BitstreamException;
import javazoom.jl.decoder.Header;
import javazoom.jl.player.Player;
import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.util.ArrayList;


class PlayMusic extends Component {

    private Player jlPlayer;
    String target="";
    ArrayList<String> pList=new ArrayList<>();
    String [] forPlayer;
    int song = 0;
    long totalSec;


    /** Mp3 lejátszó: ez olvassa be file-ból*/
    public void play(String playNow) {

            try {

            FileInputStream fileInputStream = new FileInputStream(playNow);
            BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);

           jlPlayer = new Player(bufferedInputStream);


        } catch (IOException e) {
            System.out.println("Problem playing mp3 file " + target);
            System.out.println(e.getMessage());}
        catch (Exception e) {
            System.out.println("Problem playing mp3 file " + target);
            System.out.println(e.getMessage());
        }

        new Thread() {
            public void run() {
                try {

                    jlPlayer.play();

                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }
        }.start();




    }

    /**File kiválasztása és útvonal visszaadása*/
    public String fileChooser() {
           try
        {
            JFileChooser open = new JFileChooser("C:\\Users\\Norbert\\Music\\");
            open.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            int option = open.showOpenDialog(this);
            File f1 = new File(open.getSelectedFile().getPath());

            target+=f1.getPath()+"\\";

        }
        catch(Exception e)
        {
            System.out.println(e);
        }
        System.out.println(target);
        return target;
    }

    /** Mp3 játéklista összeállítása és visszaadása ArrayListben*/
    public ArrayList<String> Mp3playList(){

        String [] fName=null;

        try{
            File targetFile = new File(target);
            fName = targetFile.list();
        }catch (Exception e){
            System.out.println(e);
        }

        for(int i=0; i< fName.length;i++){
            if(fName[i].substring(fName[i].length()-4).equals(".mp3")) {
                System.out.println(i+1 + "    " + fName[i]);
                pList.add(target+fName[i]);


            }

        }

       return pList;
    }


    /** Mp3 játéklista Stringben a Jtext-hez*/
    public String playString(){

        String str = "";

        for(String x: pList){
            str+=x.substring(x.lastIndexOf("\\")+1)+"\n";
        }

        return str;
    }

    /** Az aktuális zene a JLabelhez*/
    public String OneSong(){

        String res = pList.get(song).substring(pList.get(song).lastIndexOf("\\")+1);

        return res;
    }

    /** Zene hosszának kiszámítása*/
      public double dataMp3(String target)  {
        Header h = null;
        FileInputStream file = null;
        try {
            file = new FileInputStream(target);
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
        Bitstream bitstream = new Bitstream(file);
        try {
            h = bitstream.readFrame();
        } catch (BitstreamException ex) {
            ex.getMessage();
        }
        int size = h.calculate_framesize();
        float ms_per_frame = h.ms_per_frame();
        int maxSize = h.max_number_of_frames(10000);
        float t = h.total_ms(size);
        long tn = 0;
        try {
            tn = file.getChannel().size();
        } catch (IOException ex) {
            ex.getMessage();
        }
//System.out.println("Chanel: " + file.getChannel().size());
       /* int min = h.min_number_of_frames(500);
        System.out.println(h.total_ms((int)tn));
        System.out.println(h.total_ms((int) tn)/1000);*/
        return h.total_ms((int)tn);
    }

    /** Ezen még dolozom, loopot és thread-et használ ami miat a program nem válaszol amíg a játéklistának vége nem lesz*/
    @Deprecated()
    public void playMusic(){

        double lengthMp3=0;
        for(int i=0; i<pList.size();i++){
            System.out.println("Now playing:  " + pList.get(i));
            play(pList.get(i));
            lengthMp3 = dataMp3(pList.get(i));
            try{
                Thread.sleep((long) lengthMp3);
            }catch (Exception e){
                System.out.println(e);
            }
        }
    }

    /** Zene indítás*/
    public void playSong(){

        if(song<=0){
            song=0;
        }

        if(song>=pList.size()){
            song=pList.size()-1;
        }

        System.out.println("Now playing:  " + pList.get(song));


           play(pList.get(song));

    }

    /** Stop funkció*/
    public void close() {
        if (jlPlayer != null) jlPlayer.close();

    }
    /** Ezen még dolgozom*/
    @Deprecated
    public boolean  songMover(){

      CountDown countdown = new CountDown((int)totalSec);


     return countdown.end;

    }

    /** Az mp3 hosszának formázott visszaadása*/
    public String songLength(){

        long  lengthMp3=0;

        lengthMp3 = (long) dataMp3(pList.get(song));
        System.out.println(lengthMp3);


        long second =  ((lengthMp3 / 1000) % 60);
        long minute =  ((lengthMp3 / (1000 * 60)) % 60);
        long hour =  ((lengthMp3 / (1000 * 60 * 60)) % 24);

        String time = String.format("%02d:%02d:%02d", hour, minute, second);

        System.out.println(time);

       totalSec = minute*60 + second;
        System.out.println(totalSec);

        return time;
    }

}