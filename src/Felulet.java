import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Formatter;
import java.util.Scanner;

public class Felulet extends JFrame {

    PlayMusic playMusic = new PlayMusic();

    private Formatter x;
    private Scanner scan;
    private JTextArea jTextArea;
    private JLabel label;
    private JLabel songLabel;
    private JLabel songDur;
    private JButton album;
    private JButton open;
    private JButton clear;
    private JButton play;
    private JButton back;
    private JButton stop;
    private JButton next;
    private JScrollPane scrolltxt;

    public Felulet(){

        setLayout(null);

        label = new JLabel("Now playing: ");
        label.setBounds(4,2,100,30);
        add(label);

        songLabel = new JLabel();
        songLabel.setBounds(4,23,280,30);
        add(songLabel);

        songDur = new JLabel();
        songDur.setBounds(280,23,50,30);
        add(songDur);

        play = new JButton("Play");
        play.setBounds(4,50,80,30);
        add(play);

        back = new JButton("Back");
        back.setBounds(85,50,80,30);
        add(back);

        next = new JButton("Next");
        next.setBounds(167,50,80,30);
        add(next);

        stop = new JButton("Stop");
        stop.setBounds(250,50,80,30);
        add(stop);


        jTextArea = new JTextArea();
        scrolltxt = new JScrollPane(jTextArea);

        scrolltxt.setBounds(4, 100, 327, 300);
        add(scrolltxt);

        album = new JButton("Play All");
        album.setBounds(4,402,80,30);
        add(album);

        open = new JButton("Open");
        open.setBounds(130,402,80,30);
        add(open);

        clear = new JButton("Clear");
        clear.setBounds(250,402,80,30);
        add(clear);


        album.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                playMusic.close();
                songLabel.setText(playMusic.target);
                JOptionPane.showMessageDialog(null,"Az album végéig a program nem válaszol");

                playMusic.playMusic();

                JOptionPane.showMessageDialog(null,"Vége az albumnak!!!");

                }
        });

        open.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
               /* jTextArea.setText(null);
                playMusic.pList=null;
                playMusic.song=0;*/
                playMusic.target="";
            playMusic.fileChooser();
            playMusic.Mp3playList();
            jTextArea.append(playMusic.playString());

            }
        });

        clear.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                playMusic.target="";
                playMusic.close();
                jTextArea.setText(null);
                playMusic.pList.clear();
                playMusic.song=0;
                songLabel.setText(null);
                songDur.setText(null);



            }
        });


        play.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if(playMusic.pList.isEmpty()){
                    playMusic.fileChooser();
                    playMusic.Mp3playList();
                    jTextArea.append(playMusic.playString());
                }
                if(!playMusic.pList.isEmpty()) {
                    playMusic.close();
                    playMusic.playSong();
                    songLabel.setText(playMusic.OneSong());
                    songDur.setText(playMusic.songLength());

                }
            }
        });

        back.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {

                if(!playMusic.pList.isEmpty()){
                    playMusic.close();
                    playMusic.song--;
                    playMusic.playSong();
                    songLabel.setText(playMusic.OneSong());
                    songDur.setText(playMusic.songLength());

                }



            }
        });


        next.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {


                if(!playMusic.pList.isEmpty()) {
                    playMusic.close();
                    playMusic.song++;
                    playMusic.playSong();
                    songLabel.setText(playMusic.OneSong());
                    songDur.setText(playMusic.songLength());

                }
            }
        });

        stop.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
              playMusic.close();
              songLabel.setText(null);
              songDur.setText(null);



            }
        });

    }

}
